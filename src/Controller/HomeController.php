<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;


class HomeController extends AbstractController
{

  /**
   * @Route("/home", name="homing")
   */

  public function home(ArticleRepository $repo)
  {
    $result = $repo->getAll();
    return $this->render("home.html.twig", [
      'result' => $result
    ]);
  }
  /**
   * @Route("/article/{id}", name="new_article")
   */

  public function article(int $id, ArticleRepository $repo, Request $req)
  {
    $article = $repo->getById($id);
    // $article->handleRequest($req);

    return $this->render("article.html.twig", [
      "article" => $article
      ]);
  }
}