<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


/**
 * @Security("has_role('ROLE_ADMIN')")
 */

class UpDelGetController extends Controller
{
    /**
     * @Route("/update/{id}", name="update_id")
     */
    public function index(int $id, ArticleRepository $repo, Request $req)
    {
        $article = $repo->getById($id);

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {
            $repo->update($form->getData());
            return $this->redirectToRoute("homing");
        }
        return $this->render('up_del_get/index.html.twig', [
            'controller_name' => 'UpDelGetController', "article"=>$article,
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove_id")
     */

    public function reIndex(int $id, ArticleRepository $repo) {
        $repo->delete($id);
        return $this->redirectToRoute("homing");
    }
}
