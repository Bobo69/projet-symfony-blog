<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class EditController extends AbstractController
{

  /**
   * @Route("/edit", name="edit_user")
   */

  public function editing()
  {
    return $this->render("editAdmin.html.twig", []);
  }
}