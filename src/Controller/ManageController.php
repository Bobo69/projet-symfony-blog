<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use App\Entity\Article;


class ManageController extends AbstractController
{
  /**
   * @Route("/manage", name="manage_user")
   */
   public function manage()
   {
     return $this->render("manageUser.html.twig", []);
   }
   /**
    * @Route("/admin", name="manage_admin");
    */
    public function admin() 
    {
      return $this->render("adminContents.html.twig", []);
    }
    #########################ARTICLE#########################################
    /**
     * @Route("/createArt", name="create_article")
     */
     public function article(Request $request, ArticleRepository $repo) 
     {
      $form = $this->createForm(ArticleType::class);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
        $repo->add($form->getData());
        return $this->redirectToRoute("homing");
      }

       return $this->render("createArticle.html.twig", [
         'form' => $form->createView()
       ]);
     }
######################################CATEGORY#########################################
     /**
      * @Route("/createCat", name="create_category")
      */

      public function category()
      {
        return $this->render("createCategory.html.twig", []);
      }
}