<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use App\Entity\Article;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;





class ArticleType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
    // ->add('id', IntegerType::class)
    ->add('title', TextType::class)
    ->add('content', TextareaType::class)
    //->add('date', DateTime::class)
    // ->add('hidden', IntegerType::class)
    ->add('category', TextType::class);

  }
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults([
      "data_class" => Article::class
    ]);
  }
}