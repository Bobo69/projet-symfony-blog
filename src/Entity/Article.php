<?php

namespace App\Entity;


class Article {

  public $id;
  public $content;
  public $title;
  public $date;
  public $hidden;
  public $category;

  // public function __construct(int $paramId = null, string $paramContent = null, string $paramTitle = null, date $paramDate = null, bool $paramHidden = true, string $paramCategory = "")
  // {

    public function fromSQl(array $sql) {
      $this->id = $sql["id"];
      $this->content = $sql["content"];
      $this->title = $sql["title"];
      //$this->date = $sql["date"];
      //$this->hidden = $sql["hidden"];
      $this->category = $sql["category"];
  }
}
