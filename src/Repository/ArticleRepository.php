<?php

namespace App\Repository;

use App\Entity\Article;


class ArticleRepository{

  public function getAll() : array
  {
    $articles = [];
    try {
      $cnx = new \PDO("mysql:host=" .$_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

      $query = $cnx->prepare("SELECT * FROM article ORDER BY id DESC");
      $query->execute();

      foreach ($query->fetchAll() as $row) {
        $article = new Article();
        $article->fromSQL($row);
        $articles[] = $article;
       
      }  
    } catch (\PDOException $e) {
      dump($e);
    }
    return $articles;
  }
#############################################ADD########################################
  public function add(Article $article)
  {
    try {
      $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

      $query = $cnx->prepare("INSERT INTO article (title, content, category) VALUES (  :title, :content, :category)");


      //$query->bindValue(":id", $article->id);
      $query->bindValue(":title", $article->title);
      $query->bindValue(":content", $article->content);
      //$query->bindValue(":date", $article->date);
      //$query->bindValue(":hidden", $article->hidden);
      $query->bindValue(":category", $article->category);

      $query->execute();

      $article->id = intval($cnx->lastInsertId());

    } catch (\PDOException $e) {
      dump($e);
    }
  }
  #################################UPDATE#########################
  public function update (Article $article) {
    try {
      $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
      
      $query = $cnx->prepare("UPDATE article SET content=:content, title=:title, category=:category WHERE id=:id");

      $query->bindValue(":content", $article->content);
      $query->bindValue(":title", $article->title);
      $query->bindValue(":category", $article->category);
      $query->bindValue(":id", $article->id);
      //$query->bindValue(":id", $dog->id);

      return $query->execute();

      // $dog->id = intval($cnx->lastInsertId());pas besoin il a deja un ID

    } catch (\PDOException $e) {
      dump($e);
    }
    return false;
    }
    ###########################################DELETE########################################
    public function delete (int $id) { 
      try {
        $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
        $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        
        $query = $cnx->prepare("DELETE FROM article WHERE id=:id");
  
        $query->bindValue(":id", $id);
  
        return $query->execute();
  
    } catch (\PDOException $e) {
        dump($e);
      }
      return false;
      }
    ####################################GETbyID########################################
      public function getById(int $id): ?Article    //?Article si le chien existe ou pas
      {
        try {
          $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
          $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
          
          $query = $cnx->prepare("SELECT * FROM article WHERE id= :id");
    
          $query->bindValue(":id", $id);
    
           $query->execute();
    
          // $dog->id = intval($cnx->lastInsertId());pas besoin il a deja un ID
          $result = $query->fetchAll();

          if (count ($result) === 1) {
            $article = new Article();
            $article->fromSQL($result[0]);
            return $article;
          }

      } catch (\PDOException $e) {
          dump($e);
        }
        return null;
  }
}